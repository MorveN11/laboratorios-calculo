import matplotlib.pyplot as plt
import numpy as np

ax = plt.figure().add_subplot(projection='3d')

x, y, z = np.meshgrid(np.arange(-0.8, 1, 0.2),
                      np.arange(-0.8, 1, 0.2),
                      np.arange(-0.8, 1, 0.8))

u = np.sin(x - y)
v = np.sin(y - z)
w = np.sin(z - x)

ax.quiver(x, y, z, u, v, w, leexngth=0.1, normalize=True)

plt.show()
