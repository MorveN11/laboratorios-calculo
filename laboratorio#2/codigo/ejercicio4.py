import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-5, 5, 0.1)
y = np.arange(-5, 5, 0.1)

X, Y = np.meshgrid(x, y)

Ex = (2 * Y) - (3 * X)
Ey = (2 * Y) + (3 * X)

plt.figure(figsize=(10, 10))
plt.streamplot(X, Y, Ex, Ey, density=1.4, linewidth=None, color='#A23BEC')
plt.plot(-1, 0, '-or')
plt.plot(1, 0, '-og')
plt.title('Electromagnetic Field')

plt.grid()
plt.show()