import numpy as np
import matplotlib.pyplot as plt

x, y = np.meshgrid(np.linspace(-5, 5, 10),
                   np.linspace(-5, 5, 10))

u = y
v = -x

plt.quiver(x, y, u, v, color='g')
plt.title('Vector Field')

plt.xlim(-7, 7)
plt.ylim(-7, 7)

plt.grid()
plt.show()
