from sympy import Symbol, log, sin

x = Symbol('x')
y = log(x**2 - sin(x), 3)
print(y.diff(x))
