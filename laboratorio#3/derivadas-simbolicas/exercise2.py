from sympy import Symbol, sin, cos, E

x = Symbol('x')
y = E**cos(x) * sin(x)
print(y.diff(x))
