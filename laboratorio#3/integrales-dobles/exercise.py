from numpy.lib.scimath import sqrt
from scipy import integrate

f = lambda y, x: 2*x*y
print(integrate.dblquad(f, 0, 1, lambda x: sqrt(x), lambda x: 1 + x))
