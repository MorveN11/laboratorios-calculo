from sympy import Symbol, integrate, cos, sqrt, sin

x = Symbol('x')
print(integrate((cos(x) / sqrt(2 * sin(x) + 1)), x))
