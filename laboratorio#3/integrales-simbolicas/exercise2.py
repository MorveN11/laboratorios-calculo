from sympy import Symbol, integrate, E

x = Symbol('x')
print(integrate(E**(x**2 + 4*x + 3) * (x + 2), x))
