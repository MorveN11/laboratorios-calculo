from sympy import sqrt
from scipy.integrate import quad

function = lambda x: 1 / (1 + sqrt(x))
print(quad(function, 0, 4))
