from sympy import tan, pi
from scipy.integrate import quad

function = lambda x: tan(x) ** 2
print(quad(function, 0, pi))
