import numpy as np
import matplotlib.pyplot as plt

from numpy import power, cos, pi, e

def f1(x):
    return 5 * power(e, -2 * x) + cos(3 * x + pi / 2)

fig, ax = plt.subplots()

x = np.linspace(0, 200, 3000)
y = f1(x)

ax.plot(y, color="blue", linewidth=3, label="Ejercicio #1")
ax.set_xlim(-5, 200)
ax.set_ylim(-2, 6)

ax.set_title("Gráfica #1")
ax.set_xlabel("Valores en X")
ax.set_ylabel("Valores en Y")
ax.axhline(color="black")
ax.axvline(color="black")
ax.legend(loc="upper right", ncol=1)
fig.savefig("laboratorio#1/graficas/gráfica#1.pdf")

plt.show()
