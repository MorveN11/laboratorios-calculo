import numpy as np
import matplotlib.pyplot as plt

from numpy import power, e

def f(x, y):
    return power(e, - power(x, 2) - power(y, 2))

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

x = np.outer(np.linspace(-4, 4, 50), np.ones(50))
y = x.copy().T
z = f(x, y)

ax.plot_surface(x, y, z, cmap="viridis")
ax.set_xlim(-4, 4)
ax.set_ylim(-4, 4)
ax.set_zlim(0, 3)

ax.set_title('Ejercicio #2')
ax.set_xlabel("Valores en X")
ax.set_ylabel("Valores en Y")
ax.set_zlabel("Valores en Z")
fig.savefig("laboratorio#1/graficas/gráfica#2.pdf")

plt.show()
